import setuptools


with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="aioscraper",
    version="0.0.2",
    author="Vladimir Dranyy",
    author_email="v.dranyy@gmail.com",
    description="A simple scraper tool, can be used in interactive mode, or can be loaded from separate file. Use asyncio and aiohttp.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/vdranyy/aioscraper",
    packages=setuptools.find_packages(),
    python_requires='>=3',
    install_requires=[
        "aiohttp>=3.5",
        "lxml>=4.2"
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: POSIX :: Linux",
    ],
    entry_points={
        "console_scripts": [
            "scraper=aioscraper.scraper:main"
        ]
    },
)
