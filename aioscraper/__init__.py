"""Simple crawler package for making request,
getting response and load response.text as
lxml.html or JSON type.

There are two main feature:
    1) work in interactive shell mode:
        -- python scraper.py shell -u (URL to parse) -m
        (method GET/POST) -d (POST data);
        -- after getting and processing response switch to interactive
        console;
    2) use separate module with Spider class:
        -- python scraper.py crawl -p ("path.to.scraper.module.SpiderClass");
        -- processing and sent back results from scraper to standart output
        for now.

Planned:
    1) implement callback functions for multiple request, e.g. find all
    needed links from start URL and then pass this links to specified
    callback for processing;
    2) save results to JSON/CSV file;
    3) implement request Headers and possibility to configure it in settings
    module;
    4) implement possibility to load spider class by passing only spider
    name;
    5) implement pipelines to process each parsed Item.
"""


name = "aioscraper"

from aioscraper import base_scraper
from aioscraper import logger
from aioscraper import utils
from aioscraper import web_session

