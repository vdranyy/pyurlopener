import importlib
import os
from time import time


# Timer setup
class Timer:
    """Context manager class to calculate time of
    execution commands wrapped in this manager.
    Example:
        with Timer() as t:
            make_request_to_remote(some_url)
        print(t.duration)  # to find out response time
    """

    def __init__(self, print_at_exit=False):
        self.print_at_exit = print_at_exit

    def __enter__(self):
        self.start_time = time()
        return self

    def __exit__(self, exc_type, exc_value, exc_tb):
        self.end_time = time()
        self.exc_type = exc_type
        if self.print_at_exit:
            print(self)

    def __repr__(self):
        return "<{} duration={}{}>".format(self.__class__.__name__, self.duration, self.exception())

    def exception(self):
        return " exception={}".format(self.exc_type.__name__) if self.exc_type else ""

    @property
    def ellapsed(self):
        try:
            return round(time() - self.start_time, 2)
        except AttributeError:
            return -1

    @property
    def duration(self):
        try:
            return round(self.end_time - self.start_time, 2)
        except AttributeError:
            return -1


def load_class(full_class_string, logger):
    """Function to dynamicaly load specified Spider
    from command line -p options. Expected format must be
    'path.to.scraper.module.SpiderClass'."""

    class_data = full_class_string.split(".")
    module_path = ".".join(class_data[:-1])
    class_str = class_data[-1]

    try:
        module = importlib.import_module(module_path)
        if hasattr(module, class_str):
            return getattr(module, class_str)
    except ImportError as e:
        # logger.warning("Can't load class: {}\n{}".format(full_class_string, e.with_traceback))
        logger.warning("Can't load class: {}\n"
                       "Check your project directory. "
                       "Use scraper --help for more information.".format(full_class_string))
        return None


def make_spider_dir(name, logger):
    cwd = os.getcwd()
    spider_dir = "/".join([cwd, name])
    if not os.path.exists(spider_dir):
        os.mkdir(spider_dir)
        logger.info("Create directory for spider files: {}".format(spider_dir))
        os.mknod("/".join([spider_dir, "__init__.py"]))
        logger.info("Create __init__.py for spider files: {}".format(spider_dir))
    else:
        logger.info("{} directory for spider files exists.".format(spider_dir))

